/*
 * Decompiled with CFR 0_118.
 *
 * Could not load the following classes:
 *  org.apache.maven.model.Build
 *  org.apache.maven.model.Organization
 *  org.apache.maven.plugin.AbstractMojo
 *  org.apache.maven.plugin.MojoExecutionException
 *  org.apache.maven.plugin.MojoFailureException
 *  org.apache.maven.project.MavenProject
 *  org.codehaus.mojo.nsis.NsisGenerateProjectMojo$MessageWriter
 *  org.codehaus.plexus.util.IOUtil
 *  org.codehaus.plexus.util.StringUtils
 */
package org.codehaus.mojo.nsis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.MessageFormat;
import java.util.Date;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.StringUtils;

@Mojo( name = "generate-project", defaultPhase = LifecyclePhase.PREPARE_PACKAGE )
public class NsisGenerateProjectMojo extends AbstractMojo
{

    @Parameter( property = "nsis.projectscript", defaultValue = "${project.build.directory}/project.nsh", required = true )
    private File projectScript;

    @Parameter( defaultValue = "${project}", required = true, readonly = true )
    private MavenProject project;

    /**
     * Additional variables that are to be written to the project.nsh file
     */
    @Parameter
    private Variable[] variables;

    public void setProjectScript( File projectScript )
    {
        this.projectScript = projectScript;
    }

    public void execute() throws MojoExecutionException, MojoFailureException
    {
        FileWriter filewriter = null;
        MessageWriter out = null;
        try
        {
            try
            {
                File parentDir = this.projectScript.getParentFile();
                if ( parentDir != null && !parentDir.exists() && !parentDir.mkdirs() )
                {
                    throw new MojoFailureException( "Unable to create parent directory " + parentDir.getAbsolutePath() + " for setup script file " + this.projectScript.getAbsolutePath() );
                }
                filewriter = new FileWriter( this.projectScript );
                out = new MessageWriter( filewriter );
                out.println( "; Template for project details" );
                out.println( "; Generated by {0} from pom.xml version {1}", System.getProperty( "user.name" ), this.project.getVersion() );
                out.println( "; on date {0,date}, time {0,time}", new Date() );
                out.println( "" );
                out.println( "!define PROJECT_BASEDIR \"{0}\"", this.project.getBasedir() );
                out.println( "!define PROJECT_BUILD_DIR \"{0}\"", this.project.getBuild().getDirectory() );
                out.println( "!define PROJECT_FINAL_NAME \"{0}\"", this.project.getBuild().getFinalName() );
                out.println( "!define PROJECT_GROUP_ID \"{0}\"", this.project.getGroupId() );
                out.println( "!define PROJECT_ARTIFACT_ID \"{0}\"", this.project.getArtifactId() );
                out.println( "!define PROJECT_NAME \"{0}\"", this.project.getName() );
                out.println( "!define PROJECT_VERSION \"{0}\"", this.project.getVersion() );
                if ( StringUtils.isNotEmpty( this.project.getUrl() ) )
                {
                    out.println( "!define PROJECT_URL \"{0}\"", this.project.getUrl() );
                }
                if ( this.project.getOrganization() != null )
                {
                    out.println( "!define PROJECT_ORGANIZATION_NAME \"{0}\"", this.project.getOrganization().getName() );
                    out.println( "!define PROJECT_ORGANIZATION_URL \"{0}\"", this.project.getOrganization().getUrl() );
                    out.println( "!define PROJECT_REG_KEY \"SOFTWARE\\{0}\\{1}\\{2}\"", new Object[]
                    {
                        this.project.getOrganization().getName(), this.project.getName(), this.project.getVersion()
                    } );
                    out.println( "!define PROJECT_REG_UNINSTALL_KEY \"Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{0} {1}\"", this.project.getName(), this.project.getVersion() );
                    out.println( "!define PROJECT_STARTMENU_FOLDER \"{0}\\{1}\\{2} {3}\"", new Object[]
                    {
                        "${SMPROGRAMS}", this.project.getOrganization().getName(), this.project.getName(), this.project.getVersion()
                    } );
                }
                else
                {
                    out.println( "; The project organization section is missing from your pom.xml" );
                }
                if ( variables != null )
                {
                    for ( Variable variable : variables )
                    {
                        out.println( "!define PROJECT_{0} \"{1}\"", variable.getKey().toUpperCase(), variable.getValue() );
                    }
                }
            }
            catch ( IOException e )
            {
                throw new MojoExecutionException( "Unable to generate project script " + this.projectScript.getAbsolutePath(), e );
            }
        }
        finally
        {
            IOUtil.close( out );
            IOUtil.close( filewriter );
        }
    }

    class MessageWriter extends PrintWriter
    {

        MessageWriter( Writer out )
        {
            super( out );
        }

        public void println( String pattern, Object[] arguments )
        {
            this.println( MessageFormat.format( pattern, arguments ) );
        }

        public void println( String pattern, Object arg1 )
        {
            this.println( pattern, new Object[]
            {
                arg1
            } );
        }

        public void println( String pattern, Object arg1, Object arg2 )
        {
            this.println( pattern, new Object[]
            {
                arg1, arg2
            } );
        }
    }
}
