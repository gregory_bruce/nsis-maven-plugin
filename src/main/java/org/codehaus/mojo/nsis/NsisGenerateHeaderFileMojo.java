/**
 * Copyright 2017 MojoHaus.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.codehaus.mojo.nsis;

import java.io.File;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Created 2017-01-12 by CoolBalance Inc. www.coolbalance.com
 *
 * @author Greg Bruce, CoolBalance Inc. <gabruce@coolbalance.com>
 */
@Mojo( name = "generate-headerfile", defaultPhase = LifecyclePhase.PREPARE_PACKAGE )
public class NsisGenerateHeaderFileMojo extends NsisGenerateProjectMojo
{

    /**
     * The name of the project script.
     */
    @Parameter( property = "nsis.headerfile", defaultValue = "${project.build.directory}/project.nsh", required = true )
    private File headerFile;

    public void execute()
            throws MojoExecutionException, MojoFailureException
    {
        super.setProjectScript( headerFile );
        super.execute();
    }
}
