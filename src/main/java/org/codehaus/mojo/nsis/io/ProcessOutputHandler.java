/*
 * Decompiled with CFR 0_118.
 *
 * Could not load the following classes:
 *  org.codehaus.plexus.util.IOUtil
 */
package org.codehaus.mojo.nsis.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import org.codehaus.mojo.nsis.io.ProcessOutputConsumer;
import org.codehaus.plexus.util.IOUtil;

public class ProcessOutputHandler
        implements Runnable {

    private static final int SIZE = 1024;
    private boolean done;
    private BufferedReader in;
    private ProcessOutputConsumer consumer = null;
    private PrintWriter out = null;

    public ProcessOutputHandler(InputStream in) {
        this.in = new BufferedReader(new InputStreamReader(in), 1024);
    }

    public ProcessOutputHandler(InputStream in, PrintWriter writer) {
        this(in);
        this.out = writer;
    }

    public ProcessOutputHandler(InputStream in, PrintWriter writer, ProcessOutputConsumer consumer) {
        this(in);
        this.out = writer;
        this.consumer = consumer;
    }

    public ProcessOutputHandler(InputStream in, ProcessOutputConsumer consumer) {
        this(in);
        this.consumer = consumer;
    }

    public void close() {
        IOUtil.close((Writer) this.out);
    }

    public void flush() {
        if (this.out != null) {
            this.out.flush();
        }
    }

    public boolean isDone() {
        return this.done;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void run() {
        try {
            String s = this.in.readLine();
            while (s != null) {
                this.consumeLine(s);
                if (this.out != null) {
                    this.out.println(s);
                    this.out.flush();
                }
                s = this.in.readLine();
            }
        } catch (IOException s) {
        } finally {
            IOUtil.close((Reader) this.in);
            this.done = true;
            synchronized (this) {
                this.notifyAll();
            }
        }
    }

    public void startThread() {
        Thread thread = new Thread((Runnable) this, "ProcessOutputHandler");
        thread.start();
    }

    private void consumeLine(String line) {
        if (this.consumer != null) {
            this.consumer.consumeOutputLine(line);
        }
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
