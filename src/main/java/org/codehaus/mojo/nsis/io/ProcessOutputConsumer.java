/*
 * Decompiled with CFR 0_118.
 */
package org.codehaus.mojo.nsis.io;

public interface ProcessOutputConsumer {

    public void consumeOutputLine(String var1);
}
