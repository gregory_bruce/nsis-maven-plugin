/**
 * Copyright 2017 MojoHaus.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.codehaus.mojo.nsis;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Created 2017-01-12 by CoolBalance Inc. www.coolbalance.com
 *
 * @author Greg Bruce, CoolBalance Inc. <gabruce@coolbalance.com>
 */
@Mojo(name = "make", defaultPhase = LifecyclePhase.PREPARE_PACKAGE)
public class NsisMakeMojo extends NsisCompileMojo {

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        super.execute();
    }



}
