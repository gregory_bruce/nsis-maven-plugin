/*
 * Decompiled with CFR 0_118.
 *
 * Could not load the following classes:
 *  org.apache.maven.model.Build
 *  org.apache.maven.plugin.AbstractMojo
 *  org.apache.maven.plugin.MojoExecutionException
 *  org.apache.maven.plugin.MojoFailureException
 *  org.apache.maven.plugin.logging.Log
 *  org.apache.maven.project.MavenProject
 *  org.apache.maven.project.MavenProjectHelper
 *  org.codehaus.mojo.nsis.io.ProcessOutputConsumer
 *  org.codehaus.mojo.nsis.io.ProcessOutputHandler
 *  org.codehaus.plexus.util.FileUtils
 *  org.codehaus.plexus.util.StringUtils
 */
package org.codehaus.mojo.nsis;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Character.LINE_SEPARATOR;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectHelper;
import org.codehaus.mojo.nsis.io.ProcessOutputConsumer;
import org.codehaus.mojo.nsis.io.ProcessOutputHandler;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.StringUtils;
import org.codehaus.plexus.util.io.InputStreamFacade;
import org.codehaus.plexus.util.io.RawInputStreamFacade;

@Mojo(name = "compile", defaultPhase = LifecyclePhase.PACKAGE)
public class NsisCompileMojo
        extends AbstractMojo
        implements ProcessOutputConsumer {

    /**
     * Indicates if the execution should be disabled. If true, nothing will occur during execution.
     */
    @Parameter(property = "nsis.disabled", defaultValue = "false")
    private boolean disabled;

    /**
     * Attach Artifact Flag - can generate non installer artifact, such as an exe, that should not be attached.
     */
    @Parameter(property = "nsis.setup.attachArtifact", defaultValue = "true")
    private boolean attachArtifact;

    /**
     * The binary to execute for makensis. Default assumes that the makensis can be found in the path.
     */
    @Parameter(property = "nsis.makensis.bin", defaultValue = "${project.build.directory}/dependency/nsis/makensis.exe", required = true)
    private String makensisBin;

    /**
     * Compression settings
     */
    @Parameter(required = false)
    private Compression compression;

    /**
     * The main setup script.
     */
    @Parameter(property = "nsis.scriptfile", defaultValue = "setup.nsi", required = true, alias = "scriptFile")
    private String setupScript;

    /**
     * The generated installer exe output file.
     */
    @Parameter(property = "nsis.output.file", defaultValue = "${project.build.finalName}.exe", required = true)
    private String outputFile;

    /**
     * The Maven project itself.
     */
    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project;

    /**
     * A map of environment variables which will be passed to the execution of <code>makensis</code>
     */
    @Parameter
    private final Map<String, String> environmentVariables = new HashMap<String, String>();

    /**
     * A map of values to define on the command line
     */
    @Parameter
    private final Map<String, String> definedVariables = new HashMap<String, String>();

    @Parameter
    private String classifier;

    @Parameter(defaultValue = "2")
    private int verbose = 2;

    /**
     * Internal project helper component.
     */
    @Component
    private MavenProjectHelper projectHelper;

    private final boolean isWindows = System.getProperty("os.name").startsWith("Windows");

    private String fixPath(String path) {
        return path.replace('\\', '/').replace('/', File.separatorChar);
    }

    private String makeWindowsPath(String path) {
        return path.replace('/', '\\');
    }

    public void execute() throws MojoExecutionException, MojoFailureException {
        String makensisfolderwindowspath = makeWindowsPath(new File(fixPath(this.makensisBin)).getAbsoluteFile().getParentFile().getAbsolutePath());
        if (disabled) {
            getLog().info("MOJO is disabled. Doing nothing.");
            return;
        }
        this.validate();
        ArrayList<String> commands = new ArrayList<String>();
        commands.add(fixPath(this.makensisBin));
        File targetFile = FileUtils.resolveFile(new File(this.project.getBuild().getDirectory()), this.outputFile);
        File targetDirectory = targetFile.getParentFile();
        if (!targetDirectory.exists()) {
            try {
                FileUtils.forceMkdir(targetDirectory);
            } catch (IOException e) {
                throw new MojoExecutionException("Can't create target directory " + targetDirectory.getAbsolutePath(), e);
            }
        }
        String optPrefix = this.isWindows ? "/" : "-";
        commands.add(String.valueOf(optPrefix) + "X" + "OutFile " + StringUtils.quoteAndEscape(targetFile.getAbsolutePath(), '\''));
        commands.add(String.valueOf(optPrefix) + "V" + verbose);
        if (definedVariables != null && !definedVariables.isEmpty()) {
            for (Entry<String, String> e : definedVariables.entrySet()) {
                StringBuilder d = new StringBuilder().append(optPrefix).append("D").append(e.getKey());
                if (e.getValue() != null) {
                    d.append("=").append(e.getValue());
                }
                commands.add(d.toString());
            }
        }
        commands.add(this.setupScript);
        ProcessBuilder builder = new ProcessBuilder(commands);
        builder.directory(this.project.getBasedir());
        builder.redirectErrorStream(true);
        if (environmentVariables != null && !environmentVariables.isEmpty()) {
            //environment cannot handle null values
            builder.environment().putAll(environmentVariables);
        }
        builder.environment().put("NSISDIR", makensisfolderwindowspath);
        builder.environment().put("NSISCONFDIR", makensisfolderwindowspath);

        if (this.getLog().isDebugEnabled()) {
            this.getLog().debug(("directory:  " + builder.directory().getAbsolutePath()));
            this.getLog().debug(("commands  " + builder.command().toString()));
            if (builder.environment() != null) {
                getLog().debug("environment variables: ");
                for (Map.Entry<String, String> entry : builder.environment().entrySet()) {
                    getLog().debug("  " + entry.getKey() + ": " + entry.getValue());
                }
            }
        }
        try {
            int status;
            long start = System.currentTimeMillis();
            Process process = builder.start();
            ProcessOutputHandler output = new ProcessOutputHandler(process.getInputStream(), this);
            output.startThread();
            try {
                status = process.waitFor();
            } catch (InterruptedException e) {
                status = process.exitValue();
            }
            output.setDone(true);
            if (status != 0) {
                throw new MojoExecutionException("Execution of makensis compiler failed. See output above for details.");
            }
            long end = System.currentTimeMillis();

            consumeOutputLine("Execution completed in " + (end - start) + "ms");
            if (attachArtifact) {
                // Attach the exe to the install tasks.
                this.projectHelper.attachArtifact(this.project, "exe", null, targetFile);
            }
        } catch (IOException e) {
            throw new MojoExecutionException("Unable to execute makensis", e);
        }
    }

    public void consumeOutputLine(String line) {
        this.getLog().info(("[MAKENSIS] " + line));
    }

    private void validate() throws MojoFailureException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(this.setupScript));
            String line = reader.readLine();
            while (line != null) {
                if (line.trim().startsWith("OutFile ")) {
                    this.getLog().warn("setupScript contains the property 'OutFile'. Please move this setting to the plugin-configuration");
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            // empty catch block
        } finally {
            IOUtil.close(reader);
        }
    }

    private File processInputFile() throws MojoExecutionException {
        try {
            File scriptFileFile = new File(setupScript);
            File file = new File(project.getBuild().getDirectory(), scriptFileFile.getName());

            // ignore setting for
            if (compression != null && !compression.isDefault()) {
                String contents = FileUtils.fileRead(scriptFileFile);
                StringBuilder buf = new StringBuilder();
                buf.append("SetCompressor");
                if (compression.isDoFinal()) {
                    buf.append(" /FINAL");
                }
                if (compression.isDoSolid()) {
                    buf.append(" /SOLID");
                }
                buf.append(" ").append(compression.getType().name())
                        .append(LINE_SEPARATOR)
                        .append("SetCompressorDictSize ").append(compression.getDictionarySize())
                        .append(LINE_SEPARATOR)
                        .append(LINE_SEPARATOR)
                        .append(contents);
                InputStreamFacade is = new RawInputStreamFacade(new ByteArrayInputStream(buf.toString().getBytes("UTF-8")));
                FileUtils.copyStreamToFile(is, file);
            } else {
                FileUtils.copyFile(scriptFileFile, file);
            }
            return file;
        } catch (IOException e) {
            throw new MojoExecutionException("Unable to copy file", e);
        }
    }

    protected static File getOutputFile(File basedir, String finalName, String classifier) {
        String ncf = null;
        if (classifier == null) {
            ncf = "";
        } else if (classifier.trim().length() > 0 && !classifier.startsWith("-")) {
            ncf = "-" + classifier;
        }

        int extensionIndex = finalName.lastIndexOf('.');

        return new File(basedir, finalName.substring(0, extensionIndex) + ncf + finalName.substring(extensionIndex));
    }
}
