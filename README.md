# README #

### What is this repository for? ###

This repository started as a clone of the codehaus.org nsis-maven-plugin.

There are two varieties of this plugin floating around and they work differently. This is a start on merging the two to allow an easy upgrade. The other goal was to support more NSIS features
as well as provide maven releases of the NSIS packages that will avoid the need to install NSIS. This was driven by the desire to make it easy to create installers from Linux build systems.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Feel free to issue pull requests.
Contact gabruce@coolbalance.com with suggestions or issues.
